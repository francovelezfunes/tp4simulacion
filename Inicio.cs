﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tp4Simulacion
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
            radioButtonNormal.Checked = true;
        }

        private void btn_iniciar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPuntoRep.Text) || string.IsNullOrEmpty(txtMediaExp.Text) || string.IsNullOrEmpty(txtSimulaciones.Text) ||
                string.IsNullOrEmpty(txtMediaNormal.Text) || string.IsNullOrEmpty(txtDesviacion.Text) || string.IsNullOrEmpty(txtDesde.Text) || string.IsNullOrEmpty(txtHasta.Text))
            {
                MessageBox.Show("¡Debe ingresar todos los datos requeridos!", "¡Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //else if (Convert.ToInt32(txtLimA.Text) >= Convert.ToInt32(txtLimB.Text))
            //{
            //    MessageBox.Show("¡El limite inferior no puede ser mayor o igual al superior!", "Problema en la definición de la distribución uniforme",
            //        MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            else if (Convert.ToInt32(txtDesde.Text) >= Convert.ToInt32(txtHasta.Text))
            {
                MessageBox.Show("¡El limite inferior no puede ser mayor o igual al superior!", "Problema en la definición de los datos a visualizar",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {



                int desde, hasta, simulaciones, puntoReposicion, mediaNormal,desv, media,tipoDemanda, limiteInventario;
                desde = Convert.ToInt32(txtDesde.Text);
                hasta = Convert.ToInt32(txtHasta.Text);

                simulaciones = Convert.ToInt32(txtSimulaciones.Text);
                mediaNormal = Convert.ToInt32(txtMediaNormal.Text);
                desv = Convert.ToInt32(txtDesviacion.Text);
                media = Convert.ToInt32(txtMediaExp.Text);
                puntoReposicion = Convert.ToInt32(txtPuntoRep.Text);
                limiteInventario = Convert.ToInt32(txtLimiteInventario.Text);

                
                if (radioButtonNormal.Checked)
                    tipoDemanda = 0;
                else
                    tipoDemanda = 1;

                new Resultado(desde, hasta, simulaciones, puntoReposicion, mediaNormal, desv, media, tipoDemanda, limiteInventario);




            }

        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que quiere salir?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                this.Close();
        }
    }
}
