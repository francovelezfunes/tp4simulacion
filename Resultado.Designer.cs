﻿namespace Tp4Simulacion
{
    partial class Resultado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvResultado = new System.Windows.Forms.DataGridView();
            this.lblresultadoFinal = new System.Windows.Forms.Label();
            this.dia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rnd1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.demanda1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantDisp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rndcostoUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rndCantP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rndDemora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.demoraE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoUnitarioCarburador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoMantenimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoOrdCar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoImagen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantFaltantes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoFaltanteCarb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantSobrepasada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoSobrepasar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoTotalxCarb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalCostosDir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumCostoOrden = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumcostdeCarburador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumCostoImagen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumCantFaltante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumCostoFaltante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumCantSobrante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumCostoSobrePasar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumCostoInventario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultado)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(642, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "RESULTADO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // dgvResultado
            // 
            this.dgvResultado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvResultado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgvResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResultado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dia,
            this.rnd1,
            this.demanda1,
            this.cantDisp,
            this.rndcostoUnitario,
            this.rndCantP,
            this.cantidadP,
            this.rndDemora,
            this.demoraE,
            this.costoUnitarioCarburador,
            this.costoMantenimiento,
            this.costoOrdCar,
            this.costoImagen,
            this.cantFaltantes,
            this.costoFaltanteCarb,
            this.cantSobrepasada,
            this.costoSobrepasar,
            this.costoTotalxCarb,
            this.totalCostosDir,
            this.acumCostoOrden,
            this.acumcostdeCarburador,
            this.acumCostoImagen,
            this.acumCantFaltante,
            this.acumCostoFaltante,
            this.acumCantSobrante,
            this.acumCostoSobrePasar,
            this.acumCostoInventario,
            this.costoTotal});
            this.dgvResultado.Location = new System.Drawing.Point(12, 57);
            this.dgvResultado.Name = "dgvResultado";
            this.dgvResultado.Size = new System.Drawing.Size(1775, 668);
            this.dgvResultado.TabIndex = 7;
            // 
            // lblresultadoFinal
            // 
            this.lblresultadoFinal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblresultadoFinal.AutoSize = true;
            this.lblresultadoFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblresultadoFinal.Location = new System.Drawing.Point(21, 770);
            this.lblresultadoFinal.Name = "lblresultadoFinal";
            this.lblresultadoFinal.Size = new System.Drawing.Size(0, 24);
            this.lblresultadoFinal.TabIndex = 8;
            // 
            // dia
            // 
            this.dia.HeaderText = "Día";
            this.dia.Name = "dia";
            this.dia.ReadOnly = true;
            this.dia.Width = 50;
            // 
            // rnd1
            // 
            this.rnd1.HeaderText = "RND Demanda";
            this.rnd1.Name = "rnd1";
            this.rnd1.ReadOnly = true;
            this.rnd1.Width = 96;
            // 
            // demanda1
            // 
            this.demanda1.HeaderText = "Demanda";
            this.demanda1.Name = "demanda1";
            this.demanda1.ReadOnly = true;
            this.demanda1.Width = 78;
            // 
            // cantDisp
            // 
            this.cantDisp.HeaderText = "Cantidad Disponible";
            this.cantDisp.Name = "cantDisp";
            this.cantDisp.ReadOnly = true;
            this.cantDisp.Width = 115;
            // 
            // rndcostoUnitario
            // 
            this.rndcostoUnitario.HeaderText = "RND Intervalo";
            this.rndcostoUnitario.Name = "rndcostoUnitario";
            this.rndcostoUnitario.Width = 92;
            // 
            // rndCantP
            // 
            this.rndCantP.HeaderText = "RND Cantidad Pedida";
            this.rndCantP.Name = "rndCantP";
            this.rndCantP.ReadOnly = true;
            this.rndCantP.Width = 125;
            // 
            // cantidadP
            // 
            this.cantidadP.HeaderText = "Cantidad Pedida";
            this.cantidadP.Name = "cantidadP";
            this.cantidadP.ReadOnly = true;
            this.cantidadP.Width = 101;
            // 
            // rndDemora
            // 
            this.rndDemora.HeaderText = "RND Demora entrega";
            this.rndDemora.Name = "rndDemora";
            this.rndDemora.ReadOnly = true;
            this.rndDemora.Width = 123;
            // 
            // demoraE
            // 
            this.demoraE.HeaderText = "Demora entrega";
            this.demoraE.Name = "demoraE";
            this.demoraE.ReadOnly = true;
            this.demoraE.Width = 99;
            // 
            // costoUnitarioCarburador
            // 
            this.costoUnitarioCarburador.HeaderText = "Costo unitario carburador";
            this.costoUnitarioCarburador.Name = "costoUnitarioCarburador";
            this.costoUnitarioCarburador.Width = 137;
            // 
            // costoMantenimiento
            // 
            this.costoMantenimiento.HeaderText = "Costo Total por de carburadores";
            this.costoMantenimiento.Name = "costoMantenimiento";
            this.costoMantenimiento.ReadOnly = true;
            this.costoMantenimiento.Width = 112;
            // 
            // costoOrdCar
            // 
            this.costoOrdCar.HeaderText = "Costo de la Orden";
            this.costoOrdCar.Name = "costoOrdCar";
            this.costoOrdCar.ReadOnly = true;
            this.costoOrdCar.Width = 81;
            // 
            // costoImagen
            // 
            this.costoImagen.HeaderText = "Costo de imagen";
            this.costoImagen.Name = "costoImagen";
            this.costoImagen.ReadOnly = true;
            this.costoImagen.Width = 102;
            // 
            // cantFaltantes
            // 
            this.cantFaltantes.HeaderText = "Cantidad de faltantes";
            this.cantFaltantes.Name = "cantFaltantes";
            this.cantFaltantes.ReadOnly = true;
            this.cantFaltantes.Width = 121;
            // 
            // costoFaltanteCarb
            // 
            this.costoFaltanteCarb.HeaderText = "Costo por faltante de carburadores";
            this.costoFaltanteCarb.Name = "costoFaltanteCarb";
            this.costoFaltanteCarb.ReadOnly = true;
            this.costoFaltanteCarb.Width = 122;
            // 
            // cantSobrepasada
            // 
            this.cantSobrepasada.HeaderText = "Cantidad sobrepasada";
            this.cantSobrepasada.Name = "cantSobrepasada";
            this.cantSobrepasada.ReadOnly = true;
            this.cantSobrepasada.Width = 126;
            // 
            // costoSobrepasar
            // 
            this.costoSobrepasar.HeaderText = "Costo por sobrepaso";
            this.costoSobrepasar.Name = "costoSobrepasar";
            this.costoSobrepasar.ReadOnly = true;
            this.costoSobrepasar.Width = 118;
            // 
            // costoTotalxCarb
            // 
            this.costoTotalxCarb.HeaderText = "Costo llevar Inventario";
            this.costoTotalxCarb.Name = "costoTotalxCarb";
            this.costoTotalxCarb.ReadOnly = true;
            this.costoTotalxCarb.Width = 125;
            // 
            // totalCostosDir
            // 
            this.totalCostosDir.HeaderText = "Costo Total";
            this.totalCostosDir.Name = "totalCostosDir";
            this.totalCostosDir.ReadOnly = true;
            this.totalCostosDir.Width = 79;
            // 
            // acumCostoOrden
            // 
            this.acumCostoOrden.HeaderText = "Acum Costo  de la Orden";
            this.acumCostoOrden.Name = "acumCostoOrden";
            this.acumCostoOrden.Width = 101;
            // 
            // acumcostdeCarburador
            // 
            this.acumcostdeCarburador.HeaderText = "Acum. coste de Carburador";
            this.acumcostdeCarburador.Name = "acumcostdeCarburador";
            // 
            // acumCostoImagen
            // 
            this.acumCostoImagen.HeaderText = "Acum costo Imagen";
            this.acumCostoImagen.Name = "acumCostoImagen";
            this.acumCostoImagen.Width = 115;
            // 
            // acumCantFaltante
            // 
            this.acumCantFaltante.HeaderText = "Acum. Cant. Faltante";
            this.acumCantFaltante.Name = "acumCantFaltante";
            this.acumCantFaltante.Width = 120;
            // 
            // acumCostoFaltante
            // 
            this.acumCostoFaltante.HeaderText = "Acum. Costo Faltante";
            this.acumCostoFaltante.Name = "acumCostoFaltante";
            this.acumCostoFaltante.Width = 122;
            // 
            // acumCantSobrante
            // 
            this.acumCantSobrante.HeaderText = "Acum. Cant. Sobrante";
            this.acumCantSobrante.Name = "acumCantSobrante";
            this.acumCantSobrante.Width = 124;
            // 
            // acumCostoSobrePasar
            // 
            this.acumCostoSobrePasar.HeaderText = "Acum Costo Sobrepasar";
            this.acumCostoSobrePasar.Name = "acumCostoSobrePasar";
            this.acumCostoSobrePasar.Width = 133;
            // 
            // acumCostoInventario
            // 
            this.acumCostoInventario.HeaderText = "Acum. Costo Inv";
            this.acumCostoInventario.Name = "acumCostoInventario";
            this.acumCostoInventario.Width = 87;
            // 
            // costoTotal
            // 
            this.costoTotal.HeaderText = "Costo Total Acumulado";
            this.costoTotal.Name = "costoTotal";
            this.costoTotal.ReadOnly = true;
            this.costoTotal.Width = 130;
            // 
            // Resultado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1799, 901);
            this.Controls.Add(this.lblresultadoFinal);
            this.Controls.Add(this.dgvResultado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "Resultado";
            this.Text = "Resultado";
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvResultado;
        private System.Windows.Forms.Label lblresultadoFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn dia;
        private System.Windows.Forms.DataGridViewTextBoxColumn rnd1;
        private System.Windows.Forms.DataGridViewTextBoxColumn demanda1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantDisp;
        private System.Windows.Forms.DataGridViewTextBoxColumn rndcostoUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn rndCantP;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadP;
        private System.Windows.Forms.DataGridViewTextBoxColumn rndDemora;
        private System.Windows.Forms.DataGridViewTextBoxColumn demoraE;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoUnitarioCarburador;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoMantenimiento;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoOrdCar;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoImagen;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantFaltantes;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoFaltanteCarb;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantSobrepasada;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoSobrepasar;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoTotalxCarb;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalCostosDir;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumCostoOrden;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumcostdeCarburador;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumCostoImagen;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumCantFaltante;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumCostoFaltante;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumCantSobrante;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumCostoSobrePasar;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumCostoInventario;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoTotal;
    }
}