﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tp4Simulacion
{
    public partial class Resultado : Form
    {
        public Resultado(int desde, int hasta, int simulaciones, int puntoReposicion, int mediaNormal, int desv, int media, int tipoDemanda, int limiteInventario)
        {
            InitializeComponent();
            cargarResultados(desde, hasta, simulaciones, puntoReposicion, mediaNormal, desv, media, tipoDemanda, limiteInventario);
        }


        public void cargarResultados(int desde, int hasta, int simulaciones, int puntoReposicion, int mediaNormal, int desv, int media, int tipoDemanda, int limiteInventario)
        {
            VectorEstado anterior = new VectorEstado();
            VectorEstado actual = new VectorEstado();
            int count = 1;

            actual.SetearVariables(puntoReposicion, mediaNormal, desv, media, tipoDemanda, limiteInventario);

            if (count >= desde && count <= hasta)
                MostrarVector(actual);

            count += 1;

            while (count <= simulaciones + 1)
            {
                anterior = actual;


                actual.GenerarVector(anterior);


                if (count > desde && count <= hasta + 1)
                    MostrarVector(actual);

                count += 1;
            }

            MostrarVector(actual);

            lblresultadoFinal.Text = "El costo acumulado con un inventario de " + limiteInventario + " de capacidad  y un punto de reposición de " + puntoReposicion.ToString() + " es de: $" + String.Format("{0:N}", actual.acumCostoTotal);

            this.ShowDialog();
        }


        private void MostrarVector(VectorEstado vec)
        {

            dgvResultado.Rows.Add(vec.diaSimulacion, Math.Round(vec.rndGenerarDemanda1, 3),
                vec.cantDemandada1, vec.cantDisponible, Math.Round(vec.rndIntervaloCantCompradaCarburador, 3), Math.Round(vec.rndCantCompradaCarburador, 3), vec.cantComprada,
              Math.Round(vec.rndDiasDemoraEntrega, 3),
              vec.diasDemoraEntrega, String.Format("{0:N}", vec.costoUnitarioCarburador),
              String.Format("{0:N}", vec.costoCarburador), String.Format("{0:N}", vec.costoDeOrden),
              String.Format("{0:N}", vec.costoImagenCarburador),
              vec.cantidadFaltante, String.Format("{0:N}", vec.costoFaltanteCarburador),
               vec.cantidadSobrecarga, String.Format("{0:N}", vec.costoDeSobreCargaCarburador), 
               String.Format("{0:N}", vec.costoLlevarInventario),
               vec.costoTotal, String.Format("{0:N}", vec.acumCostoDeOrden), String.Format("{0:N}", vec.acumCostoCarburador),
               String.Format("{0:N}", vec.acumCostoImagenCarburador),
               vec.acumCantidadFaltante, String.Format("{0:N}", vec.acumCostoFaltanteCarburador),
               String.Format("{0:N}", vec.acumCantidadSobrecarga), String.Format("{0:N}", vec.acumCostoDeSobreCargaCarburador),
               String.Format("{0:N}", vec.acumCostoLlevarInventario), String.Format("{0:N}", vec.acumCostoTotal));
        }
    }
}