﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tp4Simulacion
{
    class VectorEstado
    {

        public Random rnd = new Random();
        public int cantDisponible;
        public int cantVenta;
        public int diasDemoraEntrega;
        public int costoUnitarioCarburador;
        public int costoCarburador;
        public int costoImagenCarburador;
        public int costoFaltanteCarburador;
        public int costoDeOrden;
        public int costoDeSobreCargaCarburador;
        public int costoLlevarInventario;
        public long acumCostoLlevarInventario;
        public long acumCostoCarburador = 0;
        public long acumCostoImagenCarburador = 0;
        public long acumCostoFaltanteCarburador = 0;
        public long acumCostoDeOrden = 0;
        public long acumCostoDeSobreCargaCarburador = 0;
        public int diaSimulacion;
        public int cantComprada;
        public double rndIntervaloCantCompradaCarburador;
        public double rndCantCompradaCarburador;
        public double rndDiasDemoraEntrega;
        public double rndGenerarDemanda1;
        public double rndGenerarDemanda2;
        public int? preCantDemandada1;
        public int? preCantDemandada2;
        public int? cantDemandada1;
        public int? cantDemandada2 = null;
        public int costoTotal;
        public long acumCostoTotal;
        public int cantidadFaltante;
        public long acumCantidadFaltante;
        public int cantidadSobrecarga;
        public long acumCantidadSobrecarga;


        private int _puntoReposicion;
        private int _mediaNormal;
        private int _desv;
        private int _media;
        private int _tipoDemanda;
        private int _limiteInventario;


        public void SetearVariables(int puntoReposicion, int mediaNormal, int desv, int media, int tipoDemanda, int limiteInventario)
        {
            _puntoReposicion = puntoReposicion;
            _mediaNormal = mediaNormal;
            _desv = desv;
            _media = media;
            _tipoDemanda = tipoDemanda;
            _limiteInventario = limiteInventario;
            diaSimulacion = 0;
            if (limiteInventario >= 1200)
                cantDisponible = 1200;
            else
                cantDisponible = limiteInventario;
            cantVenta = 0;


        }

        public void CalcularComprada()
        {
            //000 - 100 6.000 0-19
            //101 - 300 5.800 20-39
            //301 – 500 5.300 40-59
            //501 – 700 5.000 60-79
            //701 - 900 4.600 80-99


            rndIntervaloCantCompradaCarburador = rnd.NextDouble();
            rndCantCompradaCarburador = rnd.NextDouble();

            if (rndIntervaloCantCompradaCarburador < 0.19)
            {
                cantComprada = (int)GenerarDistribucionUniforme(0, 100);
                costoUnitarioCarburador = 6000;
            }
            else if (rndIntervaloCantCompradaCarburador < 0.39)
            {
                cantComprada = (int)GenerarDistribucionUniforme(101, 300);
                costoUnitarioCarburador = 5800;

            }
            else if (rndIntervaloCantCompradaCarburador < 0.59)
            {
                cantComprada = (int)GenerarDistribucionUniforme(301, 500);
                costoUnitarioCarburador = 5300;

            }
            else if (rndIntervaloCantCompradaCarburador < 0.79)
            {
                cantComprada = (int)GenerarDistribucionUniforme(501, 700);
                costoUnitarioCarburador = 5000;

            }
            else
            {
                cantComprada = (int)GenerarDistribucionUniforme(701, 900);
                costoUnitarioCarburador = 4600;

            }
        }


        private double GenerarDistribucionUniforme(int A, int B)
        {
            return A + rndCantCompradaCarburador * (B - A);
        }

        private void CalcularDemora()
        {
            rndDiasDemoraEntrega = rnd.NextDouble();
            diasDemoraEntrega = (int)GenerarDistribucionExponencialNegativa() == 0 ? 1 : (int)GenerarDistribucionExponencialNegativa();
        }

        private double GenerarDistribucionExponencialNegativa()
        {
            return -(_media) * Math.Log(1 - rndDiasDemoraEntrega);
        }


        private void CalcularCostos()
        {
            costoCarburador = cantComprada * costoUnitarioCarburador;
            costoImagenCarburador = cantComprada * 1500;
            costoDeOrden = 45000;
            acumCostoCarburador += costoCarburador;
            acumCostoImagenCarburador += costoImagenCarburador;
            acumCostoDeOrden += costoDeOrden;
        }


        public void GenerarVector(VectorEstado anterior)
        {

            diaSimulacion = anterior.diaSimulacion + 1;
            costoCarburador = 0;
            costoImagenCarburador = 0;
            costoUnitarioCarburador = 0;
            costoFaltanteCarburador = 0;
            costoDeOrden = 0;
            costoDeSobreCargaCarburador = 0;
            costoLlevarInventario = 0;
            costoTotal = 0;
            cantidadFaltante = 0;
            cantidadSobrecarga = 0;

            acumCostoLlevarInventario = anterior.acumCostoLlevarInventario;
            acumCostoCarburador = anterior.acumCostoCarburador;
            acumCostoImagenCarburador = anterior.acumCostoImagenCarburador;
            acumCostoFaltanteCarburador = anterior.acumCostoFaltanteCarburador;
            acumCostoDeOrden = anterior.acumCostoDeOrden;
            acumCostoDeSobreCargaCarburador = anterior.acumCostoDeSobreCargaCarburador;
            acumCostoTotal = anterior.acumCostoTotal;
            acumCantidadFaltante = anterior.acumCantidadFaltante;
            acumCantidadSobrecarga = anterior.acumCantidadSobrecarga;
            


            if (diasDemoraEntrega > 0)
                diasDemoraEntrega = anterior.diasDemoraEntrega - 1;

            if (diasDemoraEntrega == 0)
            {
                cantDisponible += cantComprada;
                cantComprada = 0;
            }

            if (cantDemandada2 != null)
            {
                cantDemandada1 = anterior.cantDemandada2;
                cantDemandada2 = null;
                rndGenerarDemanda1 = anterior.rndGenerarDemanda2;
            }
            else
                GenerarDemanda();

            cantDisponible = anterior.cantDisponible - (int)cantDemandada1;

            if (cantDisponible > _limiteInventario)
            {
                costoDeSobreCargaCarburador = (cantDisponible - _limiteInventario) * 5000;
                cantidadSobrecarga = (cantDisponible - _limiteInventario);
            }

            if (diasDemoraEntrega == 0 && _puntoReposicion > cantDisponible)
            {
                CalcularComprada();
                CalcularDemora();
                CalcularCostos();
            }

            if (cantDisponible < 0)
            {
                costoFaltanteCarburador = (-1 * cantDisponible * 7500);
                acumCostoFaltanteCarburador += costoFaltanteCarburador;
                cantidadFaltante = -1 * cantDisponible;
                cantDisponible = 0;
            }

            costoLlevarInventario = 300 * cantDisponible;
            acumCostoLlevarInventario += costoLlevarInventario;
            costoTotal = costoCarburador + costoDeOrden + costoDeSobreCargaCarburador + costoFaltanteCarburador + costoImagenCarburador + costoLlevarInventario;
            acumCostoTotal += costoTotal;
            acumCantidadSobrecarga += cantidadSobrecarga;
            acumCostoDeSobreCargaCarburador += costoDeSobreCargaCarburador;
            acumCantidadFaltante += cantidadFaltante;
        }

        private void GenerarDemanda()
        {
            rndGenerarDemanda1 = rnd.NextDouble();
            rndGenerarDemanda2 = rnd.NextDouble();
            preCantDemandada1 = (int)(Math.Sqrt(-2 * Math.Log(rndGenerarDemanda1)) * Math.Cos(2 * Math.PI * rndGenerarDemanda2) * _desv + _mediaNormal);
            preCantDemandada2 = (int)(Math.Sqrt(-2 * Math.Log(rndGenerarDemanda1)) * Math.Sin(2 * Math.PI * rndGenerarDemanda2) * _desv + _mediaNormal);
            cantDemandada1 = preCantDemandada1 < 0 ? 0 : preCantDemandada1;
            cantDemandada2 = preCantDemandada2 < 0 ? 0 : preCantDemandada2;
            //cantDemandada1 = (int)(Math.Sqrt(-2 * Math.Log(rndGenerarDemanda1)) * Math.Cos(2 * Math.PI * rndGenerarDemanda2) * _desv + _mediaNormal);
            //cantDemandada2 = (int)(Math.Sqrt(-2 * Math.Log(rndGenerarDemanda1)) * Math.Sin(2 * Math.PI * rndGenerarDemanda2) * _desv + _mediaNormal);

            if (_tipoDemanda == 1)
            {
                cantDemandada1 = (int)(cantDemandada1 * 1.3);
                cantDemandada2 = (int)(cantDemandada2 * 1.3);
            }
        }
    }
}
